{
   "AWSTemplateFormatVersion" : "2010-09-09",

  "Description" : "This CloudFormation Master template is responsible for intelligently standing up AWS resources such that dependencies are met.  It will call additional CloudFormation templates to build the VPC, bastion instance, NAT instance, CI instance, PAT instance, and necessary OpsWorks infrastructure.  A CloudFormation template for each AWS resource should be listed with the AWS::CloudFormation::Stack resource type.",

  "Resources" : {

    "escVpcStack" : {
      "Type" : "AWS::CloudFormation::Stack",
      "Properties" : {
          "TemplateURL" : "https://s3.amazonaws.com/ddigesc/cloudformation/templates/vpc.template",
          "Parameters" : {
            },
          "TimeoutInMinutes" : "600"
        }
      },

    "escBastionStack" : {
      "Type" : "AWS::CloudFormation::Stack",
      "DependsOn" : "escVpcStack",
      "Properties" : {
          "TemplateURL" : "https://s3.amazonaws.com/ddigesc/cloudformation/templates/bastion.template",
          "Parameters" : {
              "InstanceType" : "t1.micro",
              "KeyName" : "esc_cf",
              "VPCID" : { 
                "Fn::GetAtt" : [ "escVpcStack", "Outputs.VpcId" ]
              },
              "PublicSubnetId" : {
                "Fn::GetAtt" : [ "escVpcStack", "Outputs.PublicSubnetId" ]
              }
          },
          "TimeoutInMinutes" : "600"
      }
    },
      
    "escNatStack" : {
      "Type" : "AWS::CloudFormation::Stack",
      "DependsOn" : "escBastionStack",
       "Properties" : {
          "TemplateURL" : "https://s3.amazonaws.com/ddigesc/cloudformation/templates/nat.template",
          "Parameters" : {
              "InstanceType" : "m1.small",
              "KeyName" : "esc_cf",
              "VPCID" : { 
                "Fn::GetAtt" : [ "escVpcStack", "Outputs.VpcId" ]
              },
              "PublicSubnetId" : {
                "Fn::GetAtt" : [ "escVpcStack", "Outputs.PublicSubnetId" ]
              },
              "PrivateRouteTableId" : {
                "Fn::GetAtt" : [ "escVpcStack", "Outputs.RouteTableId" ]
              },
              "BastionSecurityGroupId" : {
                "Fn::GetAtt" : [ "escBastionStack", "Outputs.SecurityGroupId" ]
              }
          },
          "TimeoutInMinutes" : "600"
      }
    },
      
    "escCiStack" : {
      "Type" : "AWS::CloudFormation::Stack",
      "DependsOn" : "escBastionStack",
      "Properties" : {
          "TemplateURL" : "https://s3.amazonaws.com/ddigesc/cloudformation/templates/ci.template",
          "Parameters" : {
              "InstanceType" : "m3.large",
              "KeyName" : "esc_cf",
              "VPCID" : { 
                "Fn::GetAtt" : [ "escVpcStack", "Outputs.VpcId" ]
              },
              "PublicSubnetId" : {
                "Fn::GetAtt" : [ "escVpcStack", "Outputs.PublicSubnetId" ]
              },
              "BastionSecurityGroupId" : {
                "Fn::GetAtt" : [ "escBastionStack", "Outputs.SecurityGroupId" ]
              }
          },
          "TimeoutInMinutes" : "600"
      }
    },

    "escPortalStack" : {
      "Type" : "AWS::CloudFormation::Stack",
      "DependsOn" : "escBastionStack",
      "Properties" : {
          "TemplateURL" : "https://s3.amazonaws.com/ddigesc/cloudformation/templates/portal.template",
          "Parameters" : {
              "InstanceType" : "m3.medium",
              "KeyName" : "esc_cf",
              "VPCID" : { 
                "Fn::GetAtt" : [ "escVpcStack", "Outputs.VpcId" ]
              },
              "PublicSubnetId" : {
                "Fn::GetAtt" : [ "escVpcStack", "Outputs.PublicSubnetId" ]
              },
              "BastionSecurityGroupId" : {
                "Fn::GetAtt" : [ "escBastionStack", "Outputs.SecurityGroupId" ]
              }
          },
          "TimeoutInMinutes" : "600"
      }
    }
  }   
}